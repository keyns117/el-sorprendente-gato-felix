/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gatito;

import javax.swing.JTextArea;

/**
 *
 * @author Keynz
 */
public class Arbol 
{
     public Nodo arbol;
    public Arbol()
    {
        arbol = new Nodo("---------");
    }

    public void addNodo(String previousMove, String currentMove) 
    {
        Nodo n = new Nodo(currentMove);
        searchNodo(arbol, n, previousMove);
    }

    private void searchNodo(Nodo root, Nodo n, String move)
    {
        if(root.move.compareTo(move) == 0)
        { 
            root.leaves.add(n);
        }
        else 
            if(root.leaves.size() > 0) 
                for(int i = 0;i < root.leaves.size();i++)
                {
                    if(root.leaves.get(i).move.compareTo(move) == 0)
                    {
                        root.leaves.get(i).leaves.add(n);
                    }
                    else
                    searchNodo(root.leaves.get(i), n, move);
                }
    }

    public void printNodo(Nodo root, String move, JTextArea content) 
    {
        if(root.move.compareTo(move) == 0)
        { 
            for(int i = 0;i < root.leaves.size();i++) 
            {
                content.append(root.leaves.get(i).move + "\n");
            }
        }
        else 
            if(root.leaves.size() > 0)
                for(int i = 0;i < root.leaves.size();i++)
                    printNodo(root.leaves.get(i), move, content);
    }

    public void printArbol(Nodo root, JTextArea content) 
    {
        content.append(root.move + "\n");
        if(root.leaves.size() > 0)
            for(int i = 0;i < root.leaves.size();i++) 
            {
                printArbol(root.leaves.get(i), content);
            }
    }

    public String rotateMove(String move, int times) 
    {
        String aux;

        for(int i = 0;i < times;i++) 
        {
            aux = "";
            aux += move.charAt(2);
            aux += move.charAt(5);
            aux += move.charAt(8);
            aux += move.charAt(1);
            aux += move.charAt(4);
            aux += move.charAt(7);
            aux += move.charAt(0);
            aux += move.charAt(3);
            aux += move.charAt(6);

            move = aux;
        }

        return move;
    }

    public String invertMove(String move) 
    {
        move = move.replace("x", "1");
        move = move.replace("o", "x");
        move = move.replace("1", "o");

        return move;
    }
}
